import '@/assets/styles/globals.css';

export const metadata = {
    title: 'RentalQuest | Найдите идеальную аренду',
    description: 'Найдите свой идеальный объект аренды',
    keywords: 'аренда, поиск аренды, поиск недвижимости'
}

const MainLayout = ({ children }) => {
  return (
    <html lang='ru'>
      <body>
        <div>{children}</div>
      </body>
    </html>
  );
};

export default MainLayout;
